set nocompatible
filetype off

" include bundles
if filereadable(expand("~/.vim/conf/bundles.vim"))
  source ~/.vim/conf/bundles.vim
endif

" -------------------------------------
"  General settings
" -------------------------------------

" enable mouse
set mouse=nvi
set ttymouse=xterm2

" set timeout to a low number
set ttimeoutlen=50

" line numbers
set number

" set to auto read when a file is changed from the outside
set autoread

" Always show current position
set ruler

" Configure backspace so it acts as it should act
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

" When searching try to be smart about cases
set smartcase

" Highlight search results
set hlsearch

" Makes search act like search in modern browsers
set incsearch

" For regular expressions turn magic on
set magic

" Show matching brackets when text indicator is over them
set showmatch

" How many tenths of a second to blink when matching brackets
set mat=2

" laststatus for airline integration
set laststatus=2

" let airline handle the mode
set noshowmode

" allow hidden buffers
set hidden

" enable bufferline
let g:airline#extensions#bufferline#enabled=1

" -------------------------------------
"  Appearance settings
" -------------------------------------

syntax enable
set background=dark

let g:airline_theme='bubblegum'
let g:airline_powerline_fonts=1

" -------------------------------------
"  File & backup settings
" -------------------------------------

set nobackup
set nowb
set noswapfile

" -------------------------------------
"  Text settings
" -------------------------------------

set expandtab
set smarttab
set tabstop=2
set softtabstop=2
set shiftwidth=2
set autoindent
set nowrap
set textwidth=0

" -------------------------------------
"  Keybindings
" -------------------------------------

let mapleader = ","

" NERDTree
map <C-n> :NERDTreeToggle<CR>

" Tagbar
map <C-t> :TagbarToggle<CR>

" copy to pasteboard
vmap <C-c> "+yy<CR>

" jump around
nmap <leader><leader> <c-^>
nmap <leader><PageUp> :bp<cr>
nmap <leader><PageDown> :bn<cr>

" buffer list+jump
nmap <leader>bb :ls<CR>:buffer<Space>

" better buffer closing
nmap <leader>bc :bp\|bd #<CR>

" CtrlP
noremap <leader>p :CtrlP<cr>

" unhighlight
nnoremap <leader><space> :noh<cr>

" easier bracket traversing
nnoremap <tab> %
vnoremap <tab> %

" remove trailing spaces
cmap kt %s/ \+$//

" sudo rewrite
cmap w!! w !sudo tee % >/dev/null

" -------------------------------------
"  Miscellaneous
" -------------------------------------

" Drupal *.module and *.install files.
augroup module
  autocmd BufRead,BufNewFile *.module set filetype=php
  autocmd BufRead,BufNewFile *.install set filetype=php
  autocmd BufRead,BufNewFile *.test set filetype=php
  autocmd BufRead,BufNewFile *.inc set filetype=php
  autocmd BufRead,BufNewFile *.profile set filetype=php
  autocmd BufRead,BufNewFile *.view set filetype=php
augroup END

" Ignore some folders and files for CtrlP indexing
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\.git$\|\.yardoc\|node_modules$\|log\|tmp$',
  \ 'file': '\.so$\|\.dat$|\.DS_Store$'
  \ }

" Use The Silver Searcher https://github.com/ggreer/the_silver_searcher
if executable('ag')
  " Use Ag over Grep
  set grepprg=ag\ --nogroup\ --nocolor

  " Use ag in CtrlP for listing files. Lightning fast and respects .gitignore
  let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
endif

