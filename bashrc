# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

source ~/.sh/env
source ~/.sh/functions
source ~/.sh/aliases
source ~/.bash/completions
source ~/.sh/paths
source ~/.bash/config

# local bash aliases
if [ -f ~/.bash_aliases ]; then
  source ~/.bash_aliases
fi

# use .localrc for settings specific to one system
if [ -f ~/.localrc ]; then
  source ~/.localrc
fi
